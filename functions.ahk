keybinderNotActive()
{
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Bitte aktiviere den Keybinder! (/on)")
	SendInput, {Enter}
}

keybinderPaused()
{
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder ist pausiert. Dr�cke {FF0000}F12{D8D8D8} um ihn zu aktivieren!")
	SendInput, {Enter}
}

ThousandsSep(x, s=".") 
{
	return RegExReplace(x, "\G\d+?(?=(\d{3})+(?:\D|$))", "$0" s)
}

getDaytime()
{
	FormatTime,Uhrzeit,,HH.mm
	uhrvar = null
	if(Uhrzeit<6.00)
	{
		return "Abend"
	}
	else if(Uhrzeit<12.00)
	{
		return "Morgen"
	}
	else if(Uhrzeit<18.00)
	{
		return "Tag"
	}
	else
	{
		return "Abend"
	}	
}

IsCDM()
{
	if(isInChat())
	{
		return true
	}
	else
	{
		return false
	}
}

DownloadToString(url, encoding="utf-8")
{
  static a := "AutoHotkey/" A_AhkVersion
  if (!DllCall("LoadLibrary", "str", "wininet") || !(h := DllCall("wininet\InternetOpen", "str", a, "uint", 1, "ptr", 0, "ptr", 0, "uint", 0, "ptr")))
    return 0
  c := s := 0, o := ""
  if (f := DllCall("wininet\InternetOpenUrl", "ptr", h, "str", url, "ptr", 0, "uint", 0, "uint", 0x80003000, "ptr", 0, "ptr"))
  {
    while (DllCall("wininet\InternetQueryDataAvailable", "ptr", f, "uint*", s, "uint", 0, "ptr", 0) && s>0)
    {
      VarSetCapacity(b, s, 0)
      DllCall("wininet\InternetReadFile", "ptr", f, "ptr", &b, "uint", s, "uint*", r)
      o .= StrGet(&b, r>>(encoding="utf-16"||encoding="cp1200"), encoding)
    }
    DllCall("wininet\InternetCloseHandle", "ptr", f)
  }
  DllCall("wininet\InternetCloseHandle", "ptr", h)
  return o
}