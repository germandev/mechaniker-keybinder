~!Up::
if(movmove > 0)
{
	if(movmove == 1)
	{
		hpy := hpy - 5
		returncode := MoveDrawObject(mhp, hpx, hpy)
	}
	else if(movmove == 2)
	{
		cary := cary - 5
		returncode := MoveDrawObject(mcar, carx, cary)
	}
}
return

~!Down::
if(movmove > 0)
{
	if(movmove == 1)
	{
		hpy := hpy + 5
		returncode := MoveDrawObject(mhp, hpx, hpy)
	}
	else if(movmove == 2)
	{
		cary := cary + 5
		returncode := MoveDrawObject(mcar, carx, cary)
	}
}
return

~!Left::
if(movmove > 0)
{
	if(movmove == 1)
	{
		hpx := hpx - 5
		returncode := MoveDrawObject(mhp, hpx, hpy)
	}
	else if(movmove == 2)
	{
		carx := carx - 5
		returncode := MoveDrawObject(mcar, carx, cary)
	}
}
return

~!Right::
if(movmove > 0)
{
	if(movmove == 1)
	{
		hpx := hpx + 5
		returncode := MoveDrawObject(mhp, hpx, hpy)
	}
	else if(movmove == 2)
	{
		carx := carx + 5
		returncode := MoveDrawObject(mcar, carx, cary)
	}
}
return

~Y::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(IsPlayerInAnyVehicle() == 1)
		{
			if(GetVehicleEngineState() == 0)
			{
				if(GetVehicleLockState() == 0)
				{
					SendChat("/lock")
				}
				SendChat("/engine")
				if(GetVehicleLightState() == 0)
				{
					SendChat("/lights")
				}
			}
			else
			{
				SendChat("/engine")
				if(GetVehicleLightState() == 1)
				{				
					SendChat("/lights")
				}
			}
		}
		else
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Du sitzt in keinem Fahrzeug.")
		}
	}
}
return

~X::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(IsPlayerDriver() == 1)
		{
			SendChat("/lock")
			lockDelay = 0
		}
		else
		{
			SendChat("/carlock")		
		}
	}
}
return

~R::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
			SendInput, t{up}{enter}
	}
}
return

~F12::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else
	{
		if(paused == 0)
		{
			paused = 1
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder wurde {FF8000}PAUSIERT{D8D8D8}")
			SetTimer, Automatismen, Off
		}
		else
		{
			paused = 0
			Loop, read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
			{
				If (chatlog_zeile >= A_Index)
				{
					Continue
				}
				else
				{
					chatlog_zeile := A_Index
				}
			}
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder wurde {00FF00}AKTIVIERT{D8D8D8}")
			SetTimer, Automatismen, On
			SetTimer, Automatismen, 250
		}
	}
}
return

~Numpad0::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("/mcduty")
	}	
}
return

~Numpad1::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("Guten " . getDaytime() . ", wie kann ich Ihnen helfen?")
		Sleep, 1100
		SendChat("Darf es evtl. ein (ref)ill, ein (rep)air oder sogar beides sein?")
	}	
}
return

~Numpad2::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(baserefillModus == 0)
		{
			SendInput, t/Gib die Id des Partners ein:{Space}
			BlockGTAKeys()
			Input, partnerID, V I M,{Enter}
			Sleep, 300
			UnBlockGTAKeys()
			if(partnerID == "")
			{
				AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es wurde keine Zahl angegeben.")
			}
			else if partnerID is not Integer
			{
				AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Das ist keine Spieler ID.")
			}
			else
			{
				baserefillModus = 1
				AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Du bietest nun immer der ID " . partnerID . " den Refill f�r 1$ an.")
				AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Dr�cke ALT + Numpad 2 um dies zu beenden.")
				auftraggeber1 := ""
			}
		}
		else
		{
			SendChat("/refill " . partnerID . " 150 1")
		}
	}
}
return

~!Numpad2::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(baserefillModus == 0)
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Baserefillmodus ist nicht aktiviert!")
		}
		else
		{
			baserefillModus = 0
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Baserefillmodus wurde deaktiviert.")
		}
	}
}
return

~Numpad3::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("/accept mechanic")
	}
}
return

~!Numpad3::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		lastJobNumber = 0
		SendChat("/cancel mechanic")
	}
}
return

~Numpad4::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("/l Guten " . getDaytime() . ", wie kann ich Ihnen helfen?")
	}
}
return

~!Numpad4::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("Ihr Mechaniker " . MyName . " w�nscht Ihnen einen sch�nen " . getDaytime() . ".")
		Sleep, 1000
	}
}
return

~Numpad7::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		SendChat("/mcinfo")
	}
}
return

~NumpadDiv::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(lastCaller == "")
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es ruft dich aktuell niemand an.")
		}
		else
		{
			SendChat("/pickup")
			caller := RegExReplace(lastCaller, "[^a-zA-Z1-9]", " ")
			SendChat("Guten " . getDaytime() . " " . caller . ", Sie sprechen mit Mechaniker (Sk. " . mechaSkill . ") " . MyName . ",")
			SendChat("was kann ich f�r Sie tun?")
		}
	}
}
return

~!NumpadDiv::
if(!IsCDM())
{
	if(keyBinderActive == 0)
	{
		keybinderNotActive()
	}
	else if(paused == 1)
	{
		keybinderPaused()
	}
	else
	{
		if(lastCaller == "")
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es ruft dich aktuell niemand an.")
		}
		else
		{
			SendChat("/pickup")
			caller := RegExReplace(lastCaller, "[^a-zA-Z1-9]", " ")
			SendChat("Guten " . getDaytime() . " " . caller . ", Sie sprechen mit Mechaniker (Sk. " . mechaSkill . ") " . MyName . ",")
			SendChat("Leider bin ich im Moment besch�ftigt oder habe anderweitig zu tun.")
			Sleep, 1000
			SendChat("Ich w�nsche Ihnen noch einen sch�nen " . getDaytime() . ".")
			SendChat("/h")
			lastCaller := ""
		}
	}
}
return