#NoEnv

hmodule := DllCall("LoadLibrary", "str", "hSamp2.dll")
if(hModule == -1 || hModule == 0)
{
	MsgBox, 48, API - Fehler, Die hSamp2.dll konnte nicht gefunden werden.`nDer Keybinder wird nun beendet.
	ExitApp
}
else
{
	hmodule := DllCall("hSamp2.dll\_SearchGTA@8", "Str", "GTA:SA:MP", "int", 1)
	hmodule := DllCall("hSamp2.dll\_SearchSAMP@0")
	hmodule := DllCall("hSamp2.dll\_AnalyzeSAMP@4")
	hmodule := DllCall("hSamp2.dll\_InjectDLLAndInitGlobalD3DHooks@0")
}

CreateText(x, y, size, color, text)
{            
	return DllCall("hSamp2.dll\_CreateText@48", "Int", x, "Int", y, "Int", 500, "Int", 500, "Str", "Arial", "Int", size, "Int", "0x110", "Str", "FW_NORMAL", "UChar", "0", "UStr", text, "Int", color, "UChar", "0")
}

SetTextString(textid, text)
{
	return DllCall("hSamp2.dll\_ModifyTextString@12", "Int", textid, "UStr", text, "UChar","0")
}

MoveDrawObject(objectID, x, y)
{
	return DllCall("hSamp2.dll\_MoveDrawObject@16", "Int", objectID, "Int", x, "Int", y, "Int", 0)
}

SetDrawObjectColor(objectID, color)
{
	return DllCall("hSamp2.dll\_SetDrawObjectColor@8", "Int", objectID, "Int", color)
}

SendDirect(text)
{
	DllCall("hSamp2.dll\_SendChat@4", "Str", text)
}

BlockGTAKeys()
{
	DllCall("hSamp2.dll\_BlockGTAKeys@0")
}

UnBlockGTAKeys()
{
	DllCall("hSamp2.dll\_UnBlockGTAKeys@0")
}