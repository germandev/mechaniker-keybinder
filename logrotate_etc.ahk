Automatismen:
if(WinExist("GTA:SA:MP"))
{
	if(WinActive("GTA:SA:MP"))
	{
		if(desk == 1)
		{
			desk := 0
			Loop, read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
			{
				chatlog_zeile := A_Index
			}
		}
		Loop, read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
		{
			If (chatlog_zeile >= A_Index)
			{
				Continue
			}
			else
			{
				chatlog_zeile := A_Index
				IfInString, A_LoopReadLine, Dein Skill als Automechaniker:
				{
					RegExMatch(A_LoopReadLine, "Dein Skill als Automechaniker: (.*).",skill)
					mechaSkill = %skill1%
					if(mechaSkill == 1)
					{
						menge = 100
					}
					else if(mechaSkill == 2)
					{
						menge = 110
					}
					else if(mechaSkill == 3)
					{
						menge = 120
					}
					else if(mechaSkill == 4)
					{
						menge = 130
					}
					else 
					{
						menge = 150
					}
					AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Dein Mechanikerskill " . mechaSkill . " wurde �bernommen.")
				}
				Else IfInString, A_LoopReadLine, Server closed the connection
				{
					SetTimer, Automatismen, Off
					keyBinderActive = 0
					CurrentName := ""
					AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder wurde deaktiviert, da du gekickt wurdest.")
					firstRun = 0
				}
				Else IfInString, A_LoopReadLine, Du bist nun im Dienst als Automechaniker und wirst Notrufe empfangen.
				{
					SendChat("/showme DunkelBlau")
				}
				Else IfInString, A_LoopReadLine, Du bist nun au�er Dienst als Automechaniker und empf�ngst keine Anrufe mehr.
				{
					SendChat("/showme Unsichtbar")
				}
				Else IfInString, A_LoopReadLine, Anrufer ID:
				{
					RegExMatch(A_LoopReadLine, ", Anrufer ID: (.*)", LastCallerTemp)
					lastCaller = %LastCallerTemp1%
				}
				Else IfInString, A_LoopReadLine, SMS:
				{
					SMSSender := "Sender: " . CurrentName
					if (!InStr(A_LoopReadLine, SMSSender)) ; Hab ich die SMS selbst geschrieben?
					{
						RegExMatch(A_LoopReadLine, "Sender: (.*) \((.*)\)",LastSMSNumber)
						lastSMS = %LastSMSNumber2%
						if(lastMessage != LastSMSNumber1)
						{
							lastMessage = %LastSMSNumber1%
						}
					}
				}
				Else IfInString, A_LoopReadLine, hat aufgelegt
				{
					SendChat("/h")
					lastCaller := ""
				}
				Else IfInString, A_LoopReadLine, Du h�rst ein langweiliges Tuten
				{
					SendChat("/h")
					lastCaller := ""
				}
				Else IfInString, A_LoopReadLine, Dein Mechanikerwagen wurde freigegeben
				{
					firstRun = 0
					fuell = 0
					rep = 0
				}
				Else IfInString, A_LoopReadLine, Du kannst nur noch
				{
					IfInString, A_LoopReadLine, Liter
					{
						RegExMatch(A_LoopReadLine, "max. (.*) Liter", fill)
						StringReplace, fill1, fill1, .,,
						if(fill1 > 0)
						{
							SendChat("/mcload fuel " . fill1)
						}
					}
					Else IfInString, A_LoopReadLine, Ersatzteile
					{
						RegExMatch(A_LoopReadLine, "max. (.*) St�ck", repTemp)
						if(repTemp1 > 0)
						{
							SendChat("/mcload spareparts " . repTemp1)
						}
					}
					Sleep, 1100
				}
				Else IfInString, A_LoopReadLine, KFZ-Mechaniker %CurrentName% f�ngt an, das
				{
					RegExMatch(A_LoopReadLine, "KFZ-Mechaniker " . CurrentName . " f�ngt an, das Auto von (.*) zu reparieren.", repUser)
					rep := rep - 1
					rep_count := rep_count + 1
					rep_win := rep_win + rep_auto
					repair_av = 0
					IniWrite, %rep_count%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Repair, Count
					IniWrite, %rep_win%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Repair, Umsatz
				}
				Else IfInString, A_LoopReadLine, Litern Benzin betankt
				{
					RegExMatch(A_LoopReadLine, "Du hast (.*)'s Auto mit (.*) Litern Benzin betankt, die Summe von \$(.*) wird zu deinem PayCheck addiert.", fillUser)
					fuell := fuell - fillUser2
					if(fillUser3 != "")
					{
						StringReplace, fillUser3, fillUser3, .,,
						ref_count := ref_count + 1
						ref_win := ref_win + fillUser3
						IniWrite, %ref_count%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Refill, Count
						IniWrite, %ref_win%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Refill, Umsatz
					}
				}
				Else IfInString, A_LoopReadLine, eingeladen.
				{
					RegExMatch(A_LoopReadLine, "Du hast (.*) Ersatzteile", repTemp)
					if(repTemp1 != "")
					{
						if(rep == 0)
						{
							rep = %repTemp1%
						}
						else
						{
							rep := rep + repTemp1
						}
					}
					RegExMatch(A_LoopReadLine, "Du hast (.*) Liter f�r", fillTemp)
					StringReplace, fillTemp1, fillTemp1, .,,
					if(fillTemp1 != "")
					{
						fuell := fuell + fillTemp1
					}
				}
				Else IfInString, A_LoopReadLine, Du kannst nun mehr Benzin in die Autos der Kunden f�llen
				{
					RegExMatch(A_LoopReadLine, "Du hast jetzt Mechaniker Skill Level (.*), Du kannst nun mehr Benzin in die Autos der Kunden f�llen", skillTemp)
					mechaSkill = %skillTemp1%
					if(mechaSkill == 1)
					{
						menge = 100
					}
					else if(mechaSkill == 2)
					{
						menge = 110
					}
					else if(mechaSkill == 3)
					{
						menge = 120
					}
					else if(mechaSkill == 4)
					{
						menge = 130
					}
					else 
					{
						menge = 150
					}
					AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Dein Mechanikerskill " . mechaSkill . " wurde �bernommen.")
				}
				Else IfInString, A_LoopReadLine, sagt:
				{
					if(GetVehicleModelId() == 552)
					{
						IfInString, A_LoopReadLine, dank
						{
							return
						}
						Else IfInString, A_LoopReadLine, %CurrentName%
						{
							return
						}
						Else IfInString, A_LoopReadLine, baserefill
						{
							return
						}
						Else IfInString, A_LoopReadLine, das macht dann
						{
							return
						}	
						Else IfInString, A_LoopReadLine, keine
						{
							return
						}
						Else IfInString, A_LoopReadLine, f�r rep
						{
							return
						}
						Else IfInString, A_LoopReadLine, ref
						{
							if(baserefillModus == 1)
							{
								AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}ACHTUNG: Der Baserefillmodus ist noch aktiv. Dr�cke {FF0000}ALT + Numpad 2{D8D8D8} um diesen zu deaktivieren.")
							}
							else
							{
								IniRead,ppl_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,PPL
								maxpreis := menge * ppl_auto
								sellfuel := 20 * ppl_auto
								RegExMatch(A_LoopReadLine, "] (.*) sagt:", fillName)
								if(fuell > menge)
								{
									SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
								}
								else if(fuell > 0)
								{
									maxpreis := fuell * ppl_auto
									SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
								}
								else
								{
									SendChat("Es tut mir leid, aber ich habe kein Benzin mehr im Wagen.")
								}
								lastJobNumber := 0
							}
						}
						Else IfInString, A_LoopReadLine, fill
						{
							if(baserefillModus == 1)
							{
								AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}ACHTUNG: Der Baserefillmodus ist noch aktiv. Dr�cke {FF0000}ALT + Numpad 2{D8D8D8} um diesen zu deaktivieren.")
							}
							else
							{
								IniRead,ppl_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,PPL
								maxpreis := menge * ppl_auto
								sellfuel := 20 * ppl_auto
								RegExMatch(A_LoopReadLine, "] (.*) sagt:", fillName)
								if(fuell > menge)
								{
									SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
								}
								else if(fuell > 0)
								{
									maxpreis := fuell * ppl_auto
									SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
								}
								else
								{
									SendChat("Es tut mir leid, aber ich habe kein Benzin mehr im Wagen.")
								}
								Sleep, 1100
								SendChat("/sellfuel " . fillName1 . " " . sellfuel)
								lastJobNumber := 0
							}
						}
						Else IfInString, A_LoopReadLine, rep
						{
							if(repair_av == 1)
							{
								IniRead,rep_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,Rep
								RegExMatch(A_LoopReadLine, "] (.*) sagt:", repName)
								SendChat("Gerne repariere ich Ihr Fahrzeug, das macht dann $" . ThousandsSep(rep_auto) . " bitte.")
								SendChat("/repair " . repName1 . " " . rep_auto)
								lastJobNumber := 0
								Sleep, 1100
							}
							else
							{
								SendChat("Aktuel repariere ich noch ein Fahrzeug, bitte haben Sie etwas Geduld und fragen etwas sp�ter erneut an.")
							}
						}
						Else IfInString, A_LoopReadLine, beides
						{
							if(repair_av == 1)
							{
								IniRead,rep_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,Rep
								RegExMatch(A_LoopReadLine, "] (.*) sagt:", repName)
								SendChat("Gerne repariere ich Ihr Fahrzeug, das macht dann $" . ThousandsSep(rep_auto) . " bitte.")
								SendChat("/repair " . repName1 . " " . rep_auto)
								lastJobNumber := 0
								Sleep, 1100
							}
							else
							{
								SendChat("Aktuel repariere ich noch einFahrzeug, bitte haben Sie etwas Geduld und fragen etwas sp�ter erneut an.")
							}
							IniRead,ppl_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,PPL
							maxpreis := menge * ppl_auto
							sellfuel := 20 * ppl_auto
							RegExMatch(A_LoopReadLine, "] (.*) sagt:", fillName)
							if(fuell > menge)
							{
								SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
							}
							else if(fuell > 0)
							{
								maxpreis := fuell * ppl_auto
								SendChat("/refill " . fillName1 . " " . menge . " " . maxpreis)
							}
							else
							{
								SendChat("Es tut mir leid, aber ich habe kein Benzin mehr im Wagen.")
							}
							auftraggeber1 := ""
							lastJobNumber := 0
						}
					}
				}
				Else IfInString, A_LoopReadLine, Du kannst das Fahrzeug nur noch mit max.
				{
					RegExMatch(A_LoopReadLine, "Du kannst das Fahrzeug nur noch mit max. (\d+) Liter", fillCount)
					
					IniRead,ppl_auto,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,PPL
					
					if(baserefillModus == 1)
					{
						preis := 1
						SendChat("/refill " . partnerID . " " . fillCount1 . " " . preis)
						lastJobNumber := 0
					}
					else
					{
						if(fillCount1 > 0)
						{
							if(fill > fillcount1)
							{
								preis := fillCount1 * ppl_auto
								SendChat("Gerne betanke ich Ihr Fahrzeug mit " . fillCount1 . " Litern, das macht dann $" . ThousandsSep(preis) . " bitte.")
								SendChat("/refill " . fillName1 . " " . fillCount1 . " " . preis)
								Sleep, 1100
								auftraggeber := ""
								lastJobNumber := 0
							}
							else
							{
								preis := fillCount1 * ppl_auto
								SendChat("Gerne betanke ich Ihr Fahrzeug mit " . fillCount1 . " Litern, das macht dann $" . ThousandsSep(preis) . " bitte.")
								SendChat("/refill " . fillName1 . " " . fillCount1 . " " . preis)
								Sleep, 1100
								auftraggeber := ""
								lastJobNumber := 0
							}
							SendChat("/sellfuel " . fillName1 . " " . sellfuel)
						}
					}
				}
				Else IfInString, A_LoopReadLine, braucht einen Mechaniker
				{
					if(GetVehicleModelId() == 552)
					{
						AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es wurde ein Mechaniker gerufen, dr�cke {FF0000}Numpad 3{D8D8D8} um den Notruf anzunehmen.")
					}
				}
				Else IfInString, A_LoopReadLine, Du hast die Anfrage f�r einen Mechaniker von
				{
					RegExMatch(A_LoopReadLine, "Du hast die Anfrage f�r einen Mechaniker von (.*) angenommen", auftraggeber)
					if(auftraggeber != "")
					{
						SendChat("/number " . auftraggeber1)
					}
				}
				Else IfInString, A_LoopReadLine, , Ph:
				{
					if(auftraggeber1 != "")
					{
						RegExMatch(A_LoopReadLine, "Name: (.*), Ph: (.*)", idnummer)
						if(idnummer2 == "")
						{
							AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Die Telefonnummer konnte nicht ermittelt werden.")
						}
						else
						{
							AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Auftraggeber wird nun per SMS informiert. Solltest du es nicht innerhalb der")
							AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}60 Sekunden schaffen, kannst du Ihn via {FF0000}/timeout{D8D8D8} anrufen. Du kannst")
							AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}den angenommenen Auftrag auch mit {FF0000}Alt + Numpad 3{D8D8D8} abbrechen.")
							Sleep, 2000
							SendChat("/sms " . idnummer2 . " Ich habe Ihren Notruf �bernommen. Bitte bleiben Sie an Ihrem")
							SendChat("/sms " . idnummer2 . " derzeitigen Ort und teilen mir mit, wo genau ich Sie finde.")
							lastJobNumber = %idnummer2%
						}
					}
				}
				Else IfInString, A_LoopReadLine, Du hast nicht so viel Benzin in deinem Wagen!
				{
					SendChat("Es tut mir leid, aber ich habe nicht mehr gen�gend Benzin mit.")
				}
				Else IfInString, A_LoopReadLine, Das Fahrzeug ist vollst�ndig repariert.
				{
					repair_av = 1
				}
				Else IfInString, A_LoopReadLine, Die Reparatur wurde abgebrochen, da das Fahrzeug zu weit entfernt ist.
				{
					repair_av = 1
					SendChat("Leider haben sie sich zu weit von Dienstfahrzeug entfernt, dadurch wurde die Reparatur abgebrochen.")
				}
				Else IfInString, A_LoopReadLine, Dieser Spieler ist nicht in deiner N�he
				{
					SendChat("Sie sind leider nicht in unmittelbarter N�he, daher kann ich Ihrem Anliegen nicht folge leisten.")
				}
				Else IfInString, A_LoopReadLine, Dieser Spieler ist nicht in deiner N�he / nicht in seinem Wagen.
				{
					SendChat("Sie sind leider nicht in unmittelbarter N�he, daher kann ich Ihrem Anliegen nicht folge leisten.")
				}
				Else IfInString, A_LoopReadLine, einen Benzinkanister mit
				{
					RegExMatch(A_LoopReadLine, "\* Du hast (.*) einen Benzinkanister mit (.*) Litern f�r \$(.*) verkauft. Der Betrag wird zu deinem PayCheck addiert.", kanisterTemp)
					if(kanisterTemp1 != "")
					{
						fuell := fuell - kanisterTemp2
					}
				}
			}
		}
		if(IsPlayerDriver() == 1)
		{
			if(IsPlayerInRangeOfPoint(-1041.000244, -583.173706, 32.020741, 2) || IsPlayerInRangeOfPoint(-1034.634644, -591.500854, 32.015545, 2)) ;~ Mechator
			{
				AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Das Tor wird automatisch ge�ffnet.")
				SendChat("/mcopen")
				Sleep, 5000
			}
			if(IsPlayerInRangeOfPoint(-1046.703003, -611.600891, 32.015831, 2)) ;~ Mecha R�ckgabe
			{
				SendChat("/returncar")
				firstRun = 0
				fuell = 0
				rep = 0
			}
			if(GetVehicleModelId() == 552 && firstRun == 0)
			{
				firstRun = 1
				SendChat("/reservecar")
			}
			else if(fuell < 0)
			{
				fuell := 0
			}
			else if(rep < 0)
			{
				rep := 0
			}
			if(IsPlayerInRangeOfPoint(-1004.581848, -694.513306, 33.619835, 3) && GetVehicleModelId() == 552)
			{
				if(fuell != 1300 || rep != 20)
				{
					AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Einen Moment, dein Wagen wird automatisch bef�llt")
					SendChat("/mcload fuel")
					Sleep, 1100
					SendChat("/mcload spareparts")
					Sleep, 1100
					SendChat("/fill")
				}
			}
		}
		;~ Overlay
		;~ GetPlayerData()
		SetTextString(mhp, GetPlayerHealth())
		;~ GetZoneName(MyZone)
		coords := GetPlayerCoordinates()
		MyTown := calculateCity(coords[1], coords[2], coords[3])
		MyZone := calculateZone(coords[1], coords[2], coords[3])
		carovtext := ""
		if(!IsPlayerInAnyVehicle())
		{
			carovtext := "| Zone: " . MyZone
		}
		else
		{
			if(GetVehicleEngineState() == 0)
			{
				motor := "Aus"
			}
			else
			{
				motor := "An"
			}
			if(GetVehicleLockState() == 0)
			{
				lock := "Offen"
			}
			else
			{
				lock := "Abgeschlossen"
			}
			if(GetVehicleLightState() == 0)
			{
				light := "Aus"
			}
			else
			{
				light := "An"
			}
			carovtext := "| Geschwindigkeit: " . Round(GetVehicleSpeed(), 3) . "km/h`n| Ort: " . MyTown . " - " . MyZone . "`n| Motor: " . motor . " `n| Status: " . lock . "`n| Licht: " . light . "`n| Health: " . GetVehicleHealth() . " DL"
		}
		if(GetVehicleModelId() == 552)
		{
			carovtext := carovtext . "`n`n| Benzin im Wagen: " . ThousandsSep(fuell) . " L ($" . ppl_auto . " pro Liter)`n| Teile im Wagen: " . rep . " ($" . rep_auto . " pro Reparatur)"
		}
		if(lastJobNumber != 0)
		{
			carovtext := carovtext . "`n`n| Kunde: " . idnummer1 . " - Tel.: " . lastJobNumber
		}
		carovtext := carovtext . "`n`n| Gewinn Refill: $" . ThousandsSep(ref_win) . " (" . ref_count . ")`n| Gewinn Pepair: $" . ThousandsSep(rep_win) . " (" . ThousandsSep(rep_count) . ")"
		SetTextString(mcar,carovtext)
	}
	else
	{
		desk := 1
	}
}
return

GuiClose:
ExitApp
