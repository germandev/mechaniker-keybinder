#IfWinActive GTA:SA:MP
#SingleInstance force
#NoEnv
#UseHook

if(!A_IsAdmin)
{
	MsgBox, 64, Keybinder - Fehler, Bitte starte den Keybinder als Administrator!
	ExitApp
}

IfNotExist, %A_ScriptDir%\img\
{
	FileCreateDir, %A_ScriptDir%\img\
	URLDownloadToFile, http://mechaniker.cloudcoding.de/img/background.png, %A_ScriptDir%\img\background.png
}

IfNotExist, %A_ScriptDir%\hSamp2.dll
{
	URLDownloadToFile, http://mechaniker.cloudcoding.de/hSamp2.dll, %A_ScriptDir%\hSamp2.dll
}

#Include vars.ahk

#Include hsamp.ahk
#Include samp.ahk
#Include functions.ahk

;~ #Include updater.ahk

#Include gui.ahk

#Include hotkeys.ahk
#Include textbinds.ahk

#Include logrotate_etc.ahk