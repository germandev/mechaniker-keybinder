:?:/on::
SendInput, {Enter}
UnBlockGTAKeys()
if(keyBinderActive == 0)
{
	Loop, read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
	{
		chatlog_zeile := A_Index
	}
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder wurde aktiviert.")
	keyBinderActive = 1
	RegRead, CurrentName, HKEY_CURRENT_USER, Software\RGN\AntiCheat, RGNAC_Nickname
	MyName := RegExReplace(CurrentName, "[^a-zA-Z1-9]", " ")
	SendDirect("/skill 6")
	
	SysGet, MonX, 16 ;~ 16 -> Breite
	SysGet, MonY, 17 ;~ 17 -> H�he
	
	;~ Temp Vars
	hpxtemp := MonX - 170
	carxtemp := MonX * 0.225
	carytemp := MonY * 0.77
	;~ Temp vars
	IniRead, hpx, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, HP, X, %hpxtemp%
	IniRead, hpy, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, HP, Y, 170
	IniRead, carx, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Car, X, %carxtemp%
	IniRead, cary, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Car, Y, %carytemp%
	
	#Include overlayinit.ahk
	initZonesAndCities()
	SetTimer, Automatismen, On
	SetTimer, Automatismen, 100
}
else
{
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Keybinder ist bereits aktiviert.")
}
return

:?:/ad::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	SendInput, /ad{Space}
}
else
{
	Random, rand, 1, 4
	if(rand == 1)
	{
		SendDirect("/ad � Mecha (Skill " . mechaSkill . ") sucht neue Herausforderung �")
	} 
	else if(rand == 2)
	{
		SendDirect("/ad � Ihr habt keinen Sprit mehr? Dann meldet euch bei mir! (Skill " . mechaSkill . ") �")
	} 
	else if(rand == 3)
	{
		SendDirect("/ad � Euer Fahrzeug ist euer bester Freund, jedoch bewegt es sich kein St�ck? Call me! (Mecha Skill " . mechaSkill . ") �")
	}
	else if(rand == 4)
	{
		SendDirect("/ad � Ihr seid verzweifelt und habt kein Saft mehr? Ruft mich an und ich helfe. (Mecha Skill " . mechaSkill . ") �")
	}
}
SendInput, {Enter}
return

:?:/timeout::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	if(lastJobNumber !=  0)
	{
		SendDirect("/call " . lastJobNumber)
	}
}
return

:?:/re::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	if(lastSMS == 0 || lastSMS == "")
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es wurde niemand gefunden, der dir eine SMS geschickt hat.")
		SendInput, {Enter}
	}
	else
	{
		SendInput, /sms{Space}%lastSMS%{Space}
		input,smsinhalt,V I M L63,{Enter}
		if ErrorLevel=Max
		{
			input,zuvielsmsinhalt,V I M L63,{Enter}
			Sleep 100
			SendDirect("/sms " . lastSMS . " " zuvielsmsinhalt)
		}
	}
}
return

:?:/sms::
SendInput, /sms{Space}
Input, smsnumber, V I M, {Space}
Input, smsinhalt, V I M L63,{Enter}
if ErrorLevel=Max
{
	Input, zuvielsmsinhalt, V I M L63,{Enter}
	Sleep 100
	SendDirect("/sms " . smsnumber . " " zuvielsmsinhalt)
}
return

:?:/setrep::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der aktuelle Reparaturpreis liegt bei: $" . rep_auto)
	SendInput, /Bitte gib den neuen Reparaturpreis ein:{Space}
	BlockGTAKeys()
	Input, repPreis, V I M,{Enter}
	Sleep, 300
	UnBlockGTAKeys()
	if(repPreis == "")
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es wurde keine Zahl angegeben.")
	}
	else if repPreis is not Integer
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Das ist keine Zahl.")
	}
	else if(repPreis < 1)
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Repairpreis muss bei mind. $1 liegen.")
	}
	else
	{
		rep_auto = %repPreis%
		IniWrite,%repPreis%,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,Rep
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der neue Preis f�r eine Reparatur wurde auf $" . rep_auto . " festgelegt.")
	}
}
return

:?:/setref::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der aktuelle Refillpreis liegt bei: $" . ppl_auto)
	SendInput, /Bitte gib den neuen Refillpreis ein:{Space}
	BlockGTAKeys()
	Input, refPreis, V I M,{Enter}
	Sleep, 300
	UnBlockGTAKeys()
	if(refPreis == "")
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Es wurde keine Zahl angegeben.")
	}
	else if refPreis is not Integer
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Das ist keine Zahl.")
	}
	else if(refPreis < 1)
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der Refillpreis muss bei mind. $1 liegen.")
	}
	else
	{
		ppl_auto = %refPreis%
		IniWrite,%refPreis%,%A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini,Einstellungen,PPL
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Der neue Preis f�r eine Refill wurde auf $" . ppl_auto . " festgelegt.")
	}
}
return

:?:/sf::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	SendInput, /Wie viel Benzin:{Space}
	BlockGTAKeys()
	Input, tempFill, V I M,{Enter}
	Sleep, 300
	UnBlockGTAKeys()
	fuell = %tempFill%
}
return

:?:/sr::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	SendInput, /Wie viel Teile:{Space}
	BlockGTAKeys()
	Input, tempRep, V I M,{Enter}
	Sleep, 300
	UnBlockGTAKeys()
	rep = %tempRep%
}
return

:?:/movmove::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	if(movmove == 0)
	{
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Welches Overlay m�chtest du verschieben?")
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}1) HP - 2) Car-OV")
		SendInput, /Auswahl:{Space}
		BlockGTAKeys()
		Input, selection, V I M,{Enter}
		Sleep, 300
		UnBlockGTAKeys()
		if selection is not Integer
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Bitte gib nur die gew�nschte Zahl ein.")
		}
		else if selection > 2
		{
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Bitte verwende nur die Zahlen, die dir angezeigt werden.")
		}
		else
		{
			movmove := selection
			AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Das Overlay kann nun via ALT + Pfeiltasten verschoben werden.")
		}
	}
	else
	{
		SendInput, {Enter}
		AddChatMessage("{2E64FE}[Mechaniker]: {D8D8D8}Die Position des Overlays wurde gespeichert.")
		if(movmove == 1)
		{
			IniWrite, %hpx%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, HP, X
			IniWrite, %hpy%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, HP, Y
			movmove = 0
		}
		else if(movmove == 2)
		{
			IniWrite, %carx%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Car, X
			IniWrite, %cary%, %A_MyDocuments%\GTA San Andreas User Files\SAMP\Mechaniker\settings.ini, Car, Y
			movmove = 0
		}
	}
}
return

:?:/runter::
if(keyBinderActive == 0)
{
	keybinderNotActive()
}
else if(paused == 1)
{
	keybinderPaused()
}
else
{
	SendDirect("Auch wenn die Leiter etwas anderes vermuten l�sst, dieses Fahrzeug ist kein Kletterger�st.")
	SendDirect("Gehen Sie also bitte von meinem Fahrzeug runter. Vielen Dank!")
	SendInput, {Enter}
}
return